package main

import (
	"context"
	"sync"

	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/battle"
	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/monster"
	"gitlab.com/vajar_minigame/proto_go/gen/user"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
)

func main() {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	logger, _ := config.Build()
	suglog := logger.Sugar()
	nc, _ := nats.Connect("nats://127.0.0.1:30422")

	battleChan := make(chan *nats.Msg, 64)
	_, err := nc.ChanSubscribe("BattleEvent", battleChan)
	if err != nil {
		suglog.Fatalw("could not start nats", "error", err)
	}

	userATemplate := user.User{Username: "userA"}
	userBTemplate := user.User{Username: "userB"}

	backConn, err := grpc.Dial("localhost:80", grpc.WithInsecure())
	battleConn, err := grpc.Dial("localhost:50054", grpc.WithInsecure())
	if err != nil {
		logrus.Fatalf("failed connecting to server")
	}
	defer func() {
		backConn.Close()
		battleConn.Close()
	}()

	userClient := user.NewUserControllerClient(backConn)
	monClient := monster.NewMonsterServicesClient(backConn)
	battleClient := battle.NewBattleServiceClient(battleConn)
	ctx := context.Background()
	var wg sync.WaitGroup

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			userA, err := userClient.AddTestUser(ctx, &userATemplate)
			if err != nil {
				logrus.Fatalf("failed creating user err: %v", err)
			}
			userB, err := userClient.AddTestUser(ctx, &userBTemplate)
			if err != nil {
				logrus.Fatalf("failed creating user err: %v", err)
			}

			suglog.Infow("add test user", "userA", userA.Id, "userB", userB.Id)

			monA, _ := monClient.AddTestMon(ctx, &common.UserId{Id: userA.Id})
			monB, _ := monClient.AddTestMon(ctx, &common.UserId{Id: userB.Id})

			teamA := &battle.Team{Mons: []*common.MonId{{Id: monA.Id}}, Users: []*common.UserId{{Id: userA.Id}}}
			teamB := &battle.Team{Mons: []*common.MonId{{Id: monB.Id}}, Users: []*common.UserId{{Id: userB.Id}}}

			battle := &battle.Battle{TeamA: teamA, TeamB: teamB}

			b, err := battleClient.AddBattle(ctx, battle)
			if err != nil {
				suglog.Fatalw("error adding battle", "error", err)
			}

			battleClient.GetBattleByID(ctx, b)

			suglog.Infow("add battle", "counter", i)

		}(i)

	}

	wg.Add(1)
	go func() {

		defer wg.Done()
		for i := 0; i < 100; i++ {
			natsMsg := <-battleChan
			event := &battle.BattleEvent{}
			err := proto.Unmarshal(natsMsg.Data, event)
			if err != nil {
				suglog.Fatalw("could not parse battleevent", "error", err)
			}

			suglog.Infow("received event", "event", event.GetBattleAdded(), "counter", i)

		}
	}()

	wg.Wait()

}
