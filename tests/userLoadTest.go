package main

import (
	"context"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/proto_go/gen/user"
	"google.golang.org/grpc"
)

func main() {

	userA := &user.User{Username: "userA"}
	userB := &user.User{Username: "userB"}

	backConn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		logrus.Fatalf("failed connecting to server")
	}
	defer func() {
		backConn.Close()
	}()

	userClient := user.NewUserControllerClient(backConn)
	ctx := context.Background()

	var wg sync.WaitGroup
	for i := 0; i < 50; i++ {
		logrus.Info("test")
		wg.Add(1)
		go func() {
			defer wg.Done()
			userA, err = userClient.AddTestUser(ctx, userA)
			if err != nil {
				logrus.Fatalf("failed creating user err: %v", err)
			}
			userB, err = userClient.AddTestUser(ctx, userB)
			if err != nil {
				logrus.Fatalf("failed creating user err: %v", err)
			}
			logrus.Infof("%+v", userA)
		}()

	}

	wg.Wait()

}

func battleListener() {

}
